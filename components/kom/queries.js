// Запрос для получения списка всех сообществ
export const komunumoj_query = `
query($first: Int!, $after: String, $enDato: DateTime, $serchi: String, $tipo: String) {
  lasto: komunumoj(last: 1, tipo_Kodo: $tipo) {
    pageInfo{
      hasPreviousPage
      startCursor
      endCursor
    }
    edges { node {uuid, objId, id} } 
  }
  komunumoj(first: $first, after: $after, enDato: $enDato, serchi: $serchi, tipo_Kodo_In: $tipo,
  orderBy: ["-rating", "-aktiva_dato"]) {
    pageInfo{
      hasNextPage
      startCursor
      endCursor
    }
    edges {
      node {
        id
        objId
        uuid
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        statistiko {
          postulita
          tuta
          mia
          membraTipo
        }
        avataro {
          bildoE {
            url
          }
          bildoF {
            url
          }
        }
        kovrilo {
          bildoE {
            url
          }
        }
        statistiko {
          tuta
        }
      }
    }
  }
}`;


// Запрос для получения данных конкретного сообщества
export const kom_query = `
query($id: Float!) {
  komunumoj(objId: $id) {
    edges {
      node {
        id
        objId
        uuid
        tipo {
          kodo
          nomo {
            enhavo
          }
        }
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        statistiko {
          postulita
          tuta
          mia
          membraTipo
        }
        avataro {
          bildoE {
            url
          }
          bildoF {
            url
          }
        }
        kovrilo {
          bildoE {
            url
          }
        }
        informo {
          enhavo
        }
        informoBildo {
          bildo
          bildoMaks
        }
        kontaktuloj {
          edges {
            node {
              objId
              unuaNomo {
                enhavo
              }
              familinomo {
                enhavo
              }
              avataro {
                bildoF {
                  url
                }
              }
              kontaktaInformo
            }
          }
        }
        rajtoj
      }
    }
  }
}`;


//Запрос для вступления пользователя в сообщество
export const kunigu_query = `
mutation($kom_uuid: UUID!) {
  kuniguKomunumon(komunumoUuid: $kom_uuid) {
    status
    message
    statistiko {
      postulita
      tuta
      mia
      membraTipo
    }
  }
}
`;

//Запрос для выхода пользователя из сообщества
export const forlasu_query = `
mutation($kom_uuid: UUID!) {
  forlasuKomunumon(komunumoUuid: $kom_uuid) {
    status
    message
    statistiko {
      postulita
      tuta
      mia
      membraTipo
    }
  }
}
`;
